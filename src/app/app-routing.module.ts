import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { TODOComponent } from './components/todo/todo.component';
import { TodoResolverService } from './services/todo-resolver.service';
import { AuthGuardService } from './services/auth-guard.service';
import { SignInComponent } from './components/sign-in/sign-in.component';

const routes: Routes = [
{
  path: '',
  redirectTo: 'todos',
  pathMatch: 'full'
},
{
  path: 'sign-in',
  component: SignInComponent
},
{
  path: 'todos',
  component: TODOComponent,
  canActivate: [
    AuthGuardService,
    AuthGuardService
  ],
  resolve: {
    todos: TodoResolverService
  }
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: [
    TodoResolverService
  ]
})
export class AppRoutingModule { }
