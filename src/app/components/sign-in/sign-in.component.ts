import { Component, OnInit, NgZone } from '@angular/core';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
import { AngularFireAuth} from 'angularfire2/auth';


@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css']
})
export class SignInComponent implements OnInit {

  constructor(private _auth: AuthService,private router: Router,private thezone:NgZone) {
   
   }

   SignIn() : void {
  
    this._auth.doGoogleLogin().then( _ => this.router.navigate([ '/todos']));
    
  }

  ngOnInit() {
    if(this._auth.isLoggedIn()) this.router.navigate(['/todos']);
  }

}
