import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TODOComponent } from './todo.component';
import { FormsModule } from '@angular/forms';
import { AppComponent } from 'src/app/app.component';
import { TodoDataService } from 'src/app/services/todo-data.service';
import { ApiService } from 'src/app/services/api.service';
import { MockApiService } from 'src/app/services/mock-api.service';
import { NO_ERRORS_SCHEMA } from '@angular/core';

describe('TODOComponent', () => {
  let component: TODOComponent;
  let fixture: ComponentFixture<TODOComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule
      ],
      declarations: [
        TODOComponent
      ],
      providers: [
        TodoDataService,
        {
          provide: ApiService,
          useClass: MockApiService
        }
      ],
      schemas: [
        NO_ERRORS_SCHEMA
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TODOComponent);
    component = fixture.componentInstance;
    component.todos = [{title:"test",id:-1,complete:false}];
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
