import { Component, OnInit } from '@angular/core';
import { Todo } from 'src/app/lib/todo';
import { TodoDataService } from 'src/app/services/todo-data.service';
import { ActivatedRoute } from '@angular/router';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css']
})
export class TODOComponent implements OnInit {

  todos: Todo[] = [];

  constructor(private todoDataService: TodoDataService,
              private route: ActivatedRoute) {
    
  }

  ngOnInit() {
   
    this.route.data
    .pipe(
      map((data) => data['todos'])
    )
    .subscribe(
      (todos) => {
        this.todos = todos;
      }
    );
  }

  // Add new method to handle event emitted by TodoListHeaderComponent
  onAddTodo(todo: Todo) {
    this.todoDataService
    .addTodo(todo)
    .subscribe(
      (newTodo) => {
        this.todos = this.todos.concat(newTodo);
      }
    );
  }

  onToggleTodoComplete(todo) {
    this.todoDataService
      .toggleTodoComplete(todo)
      .subscribe(
        (updatedTodo) => {
          todo = updatedTodo;
        }
      );
  }

  onRemoveTodo(todo) {
    this.todoDataService
      .deleteTodoById(todo.id)
      .subscribe(
        (_) => {
          this.todos = this.todos.filter((t) => t.id !== todo.id);
        }
      );
  }

}
