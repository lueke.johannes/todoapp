import { Injectable } from '@angular/core';
import { environment } from '../../environments/environment';
import { HttpClient } from '@angular/common/http';
import { Todo } from '../lib/todo';
import { Observable, from } from 'rxjs';
import { map, filter, catchError, mergeMap } from 'rxjs/operators';
import { AuthService } from './auth.service';
import { AngularFireDatabase, AngularFireList, listChanges } from 'angularfire2/database';

const API_URL = environment.apiUrl;

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  list : Observable<Todo[]> = null;

  todos : Todo[] = [];

  private handleError (error: Response | any) {
    console.error('ApiService::handleError', error);
    return Observable.throw(error);
  }

  constructor(private http: HttpClient, private af: AngularFireDatabase) { 
   this.list = this.af.list<Todo>("/todos").valueChanges();
   this.list.subscribe( l => this.todos = l);
  }

  // API: GET /todos
  public getAllTodos() : Observable<Todo[]> {
    return this.http
      .get<Todo[]>(API_URL + '/todos')
      .pipe(
        map((response) => {
          return response.map((todo) => new Todo(todo));
        }),
        catchError(this.handleError)
      );
      
  }

  // API: POST /todos
  public createTodo(todo: Todo): Observable<Todo > {
    //return from(this.af.object('/todos/' + todo.id).update(todo));
    return this.http
      .post(API_URL + '/todos', todo)
      .pipe(
        map((response) => new Todo(response)),
        catchError(this.handleError)
      );
  }

  // API: GET /todos/:id
  public getTodoById(todoId: number): Observable<Todo> {
    return this.http
      .get(API_URL + '/todos/' + todoId)
      .pipe(
        map((response) => new Todo(response)),
        catchError(this.handleError)
      );
  }

  // API: PUT /todos/:id
  public updateTodo(todo: Todo): Observable<Todo > {
    //return from(this.af.object('/todos/' + todo.id).update(todo));
    return this.http
      .put(API_URL + '/todos/' + todo.id, todo)
      .pipe(
        map((response) => new Todo(response)),
        catchError(this.handleError)
      );
  }

  // DELETE /todos/:id
  public deleteTodoById(todoId: number): Observable<null > {
    //return from(this.af.object('/todos/' + todoId).remove());
    return this.http
      .delete(API_URL + '/todos/' + todoId)
      .pipe(
        map((response) => null),
        catchError(this.handleError)
      );
  }
}
