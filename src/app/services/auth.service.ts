import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth, } from 'angularfire2/auth';
import { FirebaseAuth, } from 'angularfire2';
import * as firebase from 'firebase/app';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  authState: firebase.User = null;
  
  public get db () {return firebase.firestore;}

  constructor(public afAuth: AngularFireAuth){
    const olduser = sessionStorage.getItem("user");
    if(olduser != null){
      this.authState = JSON.parse(olduser);
    }
    this.afAuth.authState.subscribe(
      state => {
        this.authState = state;
        sessionStorage.setItem("user",JSON.stringify(state));
      }
    )
  }

  doFacebookLogin(){
    return new Promise<any>((resolve, reject) => {
      let provider = new firebase.auth.FacebookAuthProvider();
      this.afAuth.auth
      .signInWithPopup(provider)
      .then(res => {
        resolve(res);
      }, err => {
        console.log(err);
        reject(err);
      })
    })
 }

 doGoogleLogin(){
  return new Promise<any>((resolve, reject) => {
    let provider = new firebase.auth.GoogleAuthProvider();
    provider.addScope('profile');
    provider.addScope('email');
    this.afAuth.auth
    .signInWithPopup(provider)
    .then(res => {
      resolve(res);
    })
  });
}

doLogout(){
  new Promise((resolve, reject) => {
    if(firebase.auth().currentUser){
      this.afAuth.auth.signOut()
      resolve();
      this.authState = null;
    }
    else{
      reject();
    }
  });
}

 isLoggedIn() : boolean {
  return this.authState != null;
 }

}
